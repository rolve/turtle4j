package oop;

import ch.trick17.turtle4j.Turtle;

public class PenWidth {

    public static void main(String[] args) {
        var turtle = new Turtle();
        turtle.setSpeed(500);
        turtle.penUp();
        turtle.forward((360 * 2) / (2 * Math.PI));
        turtle.right(90);
        turtle.penDown();

        for (int i = 0; i < 360; i++) {
            turtle.setPenWidth(i % 60 + 1);
            turtle.back(1);
            turtle.forward(3);
            turtle.right(1);
        }
    }
}
