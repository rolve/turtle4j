package oop;

import ch.trick17.turtle4j.Turtle;

public class Circle {

    public static void main(String[] args) {
        var turtle = new Turtle();
        turtle.setSpeed(1000);
        for (int i = 0; i < 360; i++) {
            turtle.forward(1);
            turtle.right(1);
        }
    }
}
