package ch.trick17.turtle4j;

import java.util.stream.Stream;

/**
 * A simple API for drawing with a Turtle. As soon as the first method is
 * called, a window will open showing the Turtle. When the window is closed, the
 * program is terminated. The Turtle starts at the center of the window, facing
 * upwards.
 */
public class TurtleGraphics {

    /*
     * All methods in here call the corresponding method in the TurtleModel
     * class. The turtle4j-impl module is added only as a runtime dependency,
     * to avoid confusion that could arise when students inadvertently import
     * the TurtleModel class directly. Hence, the reflection-based approach
     * below.
     */

    private static final String IMPL = "ch.trick17.turtle4j.impl.";

    private static final Class<?> modelClass;
    private static final Object model;

    static {
        try {
            modelClass = Class.forName(IMPL + "TurtleModel");
            model = modelClass.getConstructor().newInstance();
            var uiClass = Class.forName(IMPL + "TurtleGraphicsUI");
            var ui = uiClass.getMethod("getInstance").invoke(null);
            uiClass.getMethod("addTurtle", modelClass).invoke(ui, model);
        } catch (ReflectiveOperationException e) {
            throw new AssertionError("turtle4j library not properly configured", e);
        }
    }

    /**
     * Moves the Turtle forward by the given number of steps. One step
     * corresponds to one pixel on standard displays; for higher resolution
     * displays, the step size is scaled accordingly.
     * <p>
     * Negative values are allowed, in which case the Turtle moves backward
     * instead.
     *
     * @param steps the number of steps to move forward
     */
    public static void forward(double steps) {
        call("forward", steps);
    }

    /**
     * Moves the Turtle backward by the given number of steps. The orientation
     * of the turtle is not changed.
     * <p>
     * This method is equivalent to calling
     * {@link #forward(double) forward(...)} with a negative value.
     *
     * @param steps the number of steps to move backward
     */
    public static void back(double steps) {
        call("back", steps);
    }

    /**
     * Rotates the Turtle to the left by the given number of degrees. The
     * position of the Turtle is not changed. Negative values are allowed, in
     * which case the Turtle rotates to the right instead.
     *
     * @param degrees the number of degrees to rotate left
     */
    public static void left(double degrees) {
        call("left", degrees);
    }

    /**
     * Rotates the Turtle to the right by the given number of degrees. The
     * position of the Turtle is not changed. Negative values are allowed, in
     * which case the Turtle rotates to the left instead.
     *
     * @param degrees the number of degrees to rotate right
     */
    public static void right(double degrees) {
        call("right", degrees);
    }

    /**
     * Lifts the pen, so that the Turtle does not draw when moving, until
     * {@link #penDown()} is called.
     */
    public static void penUp() {
        call("penUp");
    }

    /**
     * Lowers the pen, so that the turtle draws again when moving.
     *
     * @see #penUp()
     */
    public static void penDown() {
        call("penDown");
    }

    /**
     * Sets the color that the Turtle draws with. The color can be specified in
     * one of the following ways:
     * <ul>
     *     <li>As a string with the name of a color, e.g. <code>"red"</code>,
     *     <code>"blue"</code>, <code>"green"</code>, etc.</li>
     *     <li>As a string with a hexadecimal color code, e.g.
     *     <code>"#990000"</code> for a dark red.</li>
     *     <li>As an RGB format string, e.g. <code>"rgb(255, 100, 0)"</code>
     *     for a bright orange.</li>
     *     <li>As an HSL format string, e.g. <code>"hsl(120, 50%, 100%)"</code>
     *     for a bright green. The first number is the hue in degrees, where 0
     *     is red, 120 is green, and 240 is blue. The second number is the
     *     saturation in percent, and the third number is the lightness in
     *     percent.</li>
     * </ul>
     * <p>
     * The default color is black.
     *
     * @param color the color to set the pen to
     */
    public static void setPenColor(String color) {
        call("setPenColor", color);
    }

    /**
     * Sets the width of the pen that the Turtle draws with. The default width
     * is the size of one step, see {@link #forward(double) forward(...)}.
     * Only positive values are allowed.
     *
     * @param width the width of the pen
     */
    public static void setPenWidth(double width) {
        call("setPenWidth", width);
    }

    /**
     * Sets the speed of the Turtle, in steps per second. The default speed is
     * 100 steps per second. Only positive values are allowed.
     *
     * @param speed the speed to set
     */
    public static void setSpeed(double speed) {
        call("setSpeed", speed);
    }

    private static void call(String method, Object... args) {
        try {
            var argTypes = Stream.of(args)
                    .map(Object::getClass)
                    .map(type -> type == Double.class ? double.class : type)
                    .toArray(Class<?>[]::new);
            modelClass.getMethod(method, argTypes).invoke(model, args);
        } catch (ReflectiveOperationException e) {
            throw new AssertionError("turtle4j library not properly configured", e);
        }
    }
}
