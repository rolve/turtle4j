package ch.trick17.turtle4j.impl;

import javafx.animation.PauseTransition;
import javafx.animation.RotateTransition;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;
import static javafx.scene.shape.StrokeLineCap.BUTT;
import static javafx.util.Duration.seconds;

public class TurtleGraphicsUI extends StackPane {

    private static final double WIDTH = 500;
    private static final double HEIGHT = 500;
    private static final double TURN_SPEED_MOVE_SPEED_RATIO = 360.0 / 100.0; // degrees/pixel
    private static final double SLEEP_AFTER_START = 0.5; // seconds
    private static final double MIN_ANIMATION_TIME = 1 / 60.0; // seconds

    private static final TurtleGraphicsUI INSTANCE = new TurtleGraphicsUI();

    public static TurtleGraphicsUI getInstance() {
        return INSTANCE;
    }

    private final Pane drawing = new Pane();

    public TurtleGraphicsUI() {
        var latch = new SimpleLatch();
        Platform.startup(() -> {
            drawing.setMaxSize(0, 0);
            getChildren().add(drawing);

            var stage = new Stage();
            stage.setTitle("Turtle");
            stage.setWidth(WIDTH);
            stage.setHeight(HEIGHT);
            stage.setScene(new Scene(this));
            stage.setOnHidden(e -> close());
            stage.show();

            latch.finishAfter(new PauseTransition(seconds(SLEEP_AFTER_START)));
        });
        latch.await();
    }

    public void addTurtle(TurtleModel t) {
        var ui = new TurtleUI();
        t.position().addListener((p, o, n) -> turtleMoved(t, ui, o, n));
        t.rotation().addListener((p, o, n) -> turtleTurned(t, ui, (double) o, (double) n));

        var latch = new SimpleLatch();
        Platform.runLater(() -> {
            ui.setTranslateX(t.position().get().getX());
            ui.setTranslateY(t.position().get().getY());
            ui.setRotate(t.rotation().get());
            getChildren().add(ui);
            latch.countDown();
        });
        latch.await();
    }

    private void turtleMoved(TurtleModel t, TurtleUI ui, Point2D oldPos, Point2D newPos) {
        var dx = newPos.getX() - oldPos.getX();
        var dy = newPos.getY() - oldPos.getY();
        var distance = sqrt(dx * dx + dy * dy);
        var time = distance / t.speed().get();
        var penDown = t.penDownProp().get();
        var penColor = t.penColor().get();
        var penWidth = t.penWidth().get();

        if (time > MIN_ANIMATION_TIME) {
            var latch = new SimpleLatch();
            Platform.runLater(() -> {
                var line = new Line(oldPos.getX(), oldPos.getY(), oldPos.getX(), oldPos.getY());
                if (penDown) {
                    line.setStroke(penColor);
                    line.setStrokeWidth(penWidth);
                    line.setStrokeLineCap(BUTT);
                    drawing.getChildren().add(line);
                }

                latch.finishAfter(new CustomTransition(seconds(time), frac -> {
                    ui.setTranslateX(oldPos.getX() + frac * dx);
                    ui.setTranslateY(oldPos.getY() + frac * dy);
                    if (penDown) {
                        line.setEndX(oldPos.getX() + frac * dx);
                        line.setEndY(oldPos.getY() + frac * dy);
                    }
                }));
            });
            latch.await();
        } else {
            Platform.runLater(() -> {
                if (penDown) {
                    var line = new Line(oldPos.getX(), oldPos.getY(), newPos.getX(), newPos.getY());
                    line.setStroke(penColor);
                    line.setStrokeWidth(penWidth);
                    line.setStrokeLineCap(BUTT);
                    drawing.getChildren().add(line);
                }
                ui.setTranslateX(newPos.getX());
                ui.setTranslateY(newPos.getY());
            });
            sleep(time);
        }
    }

    private void turtleTurned(TurtleModel t, TurtleUI ui, double oldRot, double newRot) {
        var degrees = newRot - oldRot;
        var time = abs(degrees) / (TURN_SPEED_MOVE_SPEED_RATIO * t.speed().get());

        if (time > MIN_ANIMATION_TIME) {
            var latch = new SimpleLatch();
            Platform.runLater(() -> {
                var transition = new RotateTransition(seconds(time), ui);
                transition.setByAngle(degrees);
                latch.finishAfter(transition);
            });
            latch.await();
        } else {
            Platform.runLater(() -> ui.setRotate(newRot));
            sleep(time);
        }
    }

    private void close() {
        System.exit(0);
    }

    private void sleep(double seconds) {
        var start = System.nanoTime();
        while (System.nanoTime() - start < 1_000_000_000 * seconds) {
            Thread.onSpinWait();
        }
    }
}
