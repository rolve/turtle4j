package ch.trick17.turtle4j.impl;

import javafx.beans.property.*;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;

import static java.lang.Double.isFinite;
import static java.lang.Math.*;

public class TurtleModel {

    private static final Color INITIAL_PEN_COLOR = Color.BLACK;
    private static final int INITIAL_PEN_WIDTH = 1;
    private static final double INITIAL_SPEED = 100; // pixels/s

    private final ObjectProperty<Point2D> position = new SimpleObjectProperty<>(new Point2D(0, 0));
    private final DoubleProperty rotation = new SimpleDoubleProperty(-90);
    private final BooleanProperty penDown = new SimpleBooleanProperty(true);
    private final ObjectProperty<Color> penColor = new SimpleObjectProperty<>(INITIAL_PEN_COLOR);
    private final DoubleProperty penWidth = new SimpleDoubleProperty(INITIAL_PEN_WIDTH);
    private final DoubleProperty speed = new SimpleDoubleProperty(INITIAL_SPEED);

    public void forward(double steps) {
        if (!isFinite(steps)) {
            throw new IllegalArgumentException("steps must be finite, but is " + steps);
        }
        var dx = cos(toRadians(rotation.get())) * steps;
        var dy = sin(toRadians(rotation.get())) * steps;
        position.set(position.get().add(dx, dy));
    }

    public void back(double steps) {
        if (!isFinite(steps)) {
            throw new IllegalArgumentException("steps must be finite, but is " + steps);
        }
        forward(-steps);
    }

    public void left(double degrees) {
        if (!isFinite(degrees)) {
            throw new IllegalArgumentException("degrees must be finite, but is " + degrees);
        }
        right(-degrees);
    }

    public void right(double degrees) {
        if (!isFinite(degrees)) {
            throw new IllegalArgumentException("degrees must be finite, but is " + degrees);
        }
        rotation.set(rotation().get() + degrees);
    }

    public void penUp() {
        penDown.set(false);
    }

    public void penDown() {
        penDown.set(true);
    }

    public void setPenColor(String color) {
        try {
            penColor.set(Color.valueOf(color));
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("invalid color specification: '" + color + "'");
        }
    }

    public void setPenWidth(double width) {
        if (!isFinite(width) || width <= 0) {
            throw new IllegalArgumentException("width must be finite and positive, but is " + width);
        }
        penWidth.set(width);
    }

    public void setSpeed(double speed) {
        if (!isFinite(speed) || speed <= 0) {
            throw new IllegalArgumentException("speed must be finite and positive, but is " + speed);
        }
        this.speed.set(speed);
    }

    ReadOnlyObjectProperty<Point2D> position() {
        return position;
    }

    ReadOnlyDoubleProperty rotation() {
        return rotation;
    }

    ReadOnlyBooleanProperty penDownProp() {
        return penDown;
    }

    ReadOnlyObjectProperty<Color> penColor() {
        return penColor;
    }

    ReadOnlyDoubleProperty penWidth() {
        return penWidth;
    }

    ReadOnlyDoubleProperty speed() {
        return speed;
    }
}
