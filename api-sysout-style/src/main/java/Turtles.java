import ch.trick17.turtle4j.Turtle;

/**
 * A class that provides a {@link Turtle} instance named {@code tom}. With this
 * class you can write <code>System.out.println</code>-style Turtle code, like
 * this:
 *
 * <pre>
 * Turtles.tom.forward(100);
 * Turtles.tom.left(120);
 * Turtles.tom.forward(100);
 * </pre>
 */
public class Turtles {
    public static final Turtle tom = new Turtle();
}
